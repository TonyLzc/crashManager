#-------------------------------------------------
#
# Project created by QtCreator 2019-03-22T09:45:23
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DumpTest
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
        main.cpp \
        mainwindow.cpp

HEADERS += \
        mainwindow.h \

FORMS += \
        mainwindow.ui



include(crashManager/DumpManager.pri)

