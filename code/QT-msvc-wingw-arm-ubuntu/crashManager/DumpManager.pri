#前两行意识意思为在release版本中增加debug信息；第三行意思为release版本中去掉-s参数，这样就生成对应符号表，可以调试跟踪；
QMAKE_CXXFLAGS_RELEASE += -g
QMAKE_CFLAGS_RELEASE += -g
QMAKE_LFLAGS_RELEASE = -mthreads -Wl,

HEADERS += \
            $$PWD/crashManager.h

win32{
    include($$PWD/winCrash/winCrash.pri)
    #message('win32')
}
unix{
    include($$PWD/linuxCrash/googleCrash.pri)
    #message('unix')
}


greaterThan(QT_MAJOR_VERSION,4){
        TARGET_ARCH=$${QT_ARCH}
}else{
        TARGET_ARCH=$${QMAKE_HOST.arch}
}

contains(TARGET_ARCH, x86_64){
   message('64')
    CONFIG(debug,debug|release){
       message('debug')
    }else{
       message('release')
    }
}else{
   message('32')
    CONFIG(debug,debug|release){
       message('debug')
    }else{
       message('release')
    }
}




