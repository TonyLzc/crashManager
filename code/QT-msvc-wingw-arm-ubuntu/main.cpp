﻿#include "mainwindow.h"
#include <QApplication>
#include <QDebug>
#include "crashManager/crashManager.h"


int main(int argc, char *argv[])
{
    setCrashManager();
    QApplication a(argc, argv);

    MainWindow w;
    w.show();

    return a.exec();
}
