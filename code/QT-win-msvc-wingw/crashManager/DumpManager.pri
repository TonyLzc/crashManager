#前两行意识意思为在release版本中增加debug信息；第三行意思为release版本中去掉-s参数，这样就生成对应符号表，可以调试跟踪；
QMAKE_CXXFLAGS_RELEASE += -g
QMAKE_CFLAGS_RELEASE += -g
QMAKE_LFLAGS_RELEASE = -mthreads -Wl,

HEADERS += \
            $$PWD/crashManager.h

win32{
    include($$PWD/winCrash/winCrash.pri)
    #message('win32')
}
unix{
    #include($$PWD/linuxCrash/googleCrash.pri)
    #message('unix')
}


