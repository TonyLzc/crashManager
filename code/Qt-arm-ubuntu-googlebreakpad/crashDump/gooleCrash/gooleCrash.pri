#-------------------------------------------------
#
# Project created by QtCreator 2022-03-17T20:01:33
#
#-------------------------------------------------

QT       += network

CONFIG += c++11

SOURCES += \
    $$PWD//handler/QBreakpadHandler.cpp \
    $$PWD//handler/QBreakpadHttpUploader.cpp

HEADERS += \
    $$PWD//handler/QBreakpadHttpUploader.h \
    $$PWD//handler/QBreakpadHandler.h \
    $$PWD//handler/singletone/singleton.h \
    $$PWD//handler/singletone/call_once.h


#link Breakpad library
#arm4.4.3
#unix:!macx: LIBS += -L$$PWD/arm4.4.3/lib/ -lbreakpad_client
#INCLUDEPATH += $$PWD/arm4.4.3/include/
#DEPENDPATH += $$PWD/arm4.4.3/include/
#unix:!macx: PRE_TARGETDEPS += $$PWD/arm4.4.3/lib/libbreakpad_client.a
#ubuntu64bit 虚拟机
unix:!macx: LIBS += -L$$PWD/ubuntu64/lib/ -lbreakpad_client
INCLUDEPATH += $$PWD/ubuntu64/include/
DEPENDPATH += $$PWD/ubuntu64/include/
unix:!macx: PRE_TARGETDEPS += $$PWD/ubuntu64/lib/libbreakpad_client.a

CONFIG -= app_bundle
CONFIG += debug_and_release warn_on
CONFIG += thread exceptions rtti stl


OBJECTS_DIR = _build/obj
MOC_DIR = _build

#加入调试信息
QMAKE_CFLAGS_RELEASE += -g
QMAKE_CXXFLAGS_RELEASE += -g
#禁止优化
QMAKE_CFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE -= -O2

#release在最后link时默认有"-s”参数，表示"Omit all symbol information from the output file"，因此要去掉该参数
#QMAKE_LFLAGS_RELEASE = -mthreads -Wl   #此行经过测试可用可不用
#参考自：https://blog.csdn.net/dgj8300/article/details/78450638
