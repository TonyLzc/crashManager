﻿#include <QCoreApplication>
#include <iostream>
#include "client/linux/handler/exception_handler.h"
#include "gooleCrash/handler/QBreakpadHandler.h"
/* 触发crash来测试 */
void crash11111()
{
    volatile int* a = (int*)(NULL);
    *a = 1;
}

int main(int argc, char* argv[])
{
    QCoreApplication app(argc, argv);

    QCoreApplication::setApplicationName("AppName");
    QCoreApplication::setApplicationVersion("1.0");
    QCoreApplication::setOrganizationName("OrgName");
    QCoreApplication::setOrganizationDomain("name.org");

    QBreakpadInstance.setDumpPath("crashes");
    crash11111();

    return app.exec();
}
